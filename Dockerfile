FROM openjdk:11.0.5-jre-slim
RUN useradd springboot
EXPOSE 8000
USER springboot
COPY build/libs/query-performance-*.jar /app.jar
ENTRYPOINT exec java -XX:+UnlockExperimentalVMOptions -Xdebug -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8000 -Djava.security.egd=file:/dev/./urandom -jar /app.jar