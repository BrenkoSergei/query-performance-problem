# Query Execution Service

## Preface
In company ABC we run quite a few queries against our databases, and the queries have different performance characteristics. We frequently need to work on optimizing them. We want to build a system that allow us to compare different versions of the same query and be able to benchmark the performance of its versions.

## Solution Design
The following points were taken into account during the solution design:

1. The service must be able to execute a performance test and measure the time for each query to complete;
2. List of available environments (such as database installation) must be configurable using REST API;
3. Exactly one of the performance tests can execute at any point of time against a particular environment. Count of possible concurrent requests might be increased in future;
4. Tests against different environments can be executed in parallel;
5. User should be able to run a bunch of queries, and each of the queries might be run against a list of environments;
6. Potentially the system might be extended to run queries against NoSQL databases and\or REST services;
7. The system must ensure that if an execution scenario was successfully submitted, then it will be executed even if all the system instances are terminated during the processing;

In order to fit the requirements, the following tools were used:

1. Apache Kafka (+ Zookeeper). Concurrency on a service level is achieved by using the message broker. Every environment has a separate Kafka topic with one partition (potentially might be increased). Pattern for the topics is 'query_execution_<environment_name>'. The service application uses the ConcurrentKafkaListenerContainerFactory (together with 'Sticky' partition assignment strategy) and a Kafka Listener for the topic pattern (by.branko.query.consumer.ExecutionConsumer). By using this configuration we can guarantee that all user requests will be distributed as much as possible.
2. MongoDB - the primary storage of the application. It is deployed as a Replica Set, since it is necessary for ACID transaction support and Apache Kafka Source Connector (described below). Stores information about environments, scenarios, test executions, and has a temporary storage for messages that will be submitted into Apache Kafka. 
3. Apache Kafka Source Connector for MongoDB. The application must ensure that operation with the database and sending messages into the message broker are performed atomically. In order to achieve this, first of all we save the Kafka messages into a separate MongoDB collection (as a part of the main transaction). Then the Source Connector utilizes MongoDB's replication protocol to tail transactions log for the 'messages' collection, and sends inserted documents into a particular Apache Kafka topic. On application side we have a Kafka Listener that relays the messages into required topic. According to the contracts of the MongoDB's replication protocol and Apache Kafka, we can guarantee that operations with the database and the message broker are performed atomically.
4. Redis. Apache Kafka consumers have a configurable minimum throughput (records in milliseconds). If a consumer does not fit the throughput, it is excluded from Consumer Group and his current messages might be processed more than once. In order to comply the concurrency requirement (Exactly one of the performance tests can execute at any point of time against a particular environment) we need a distributed locking mechanism. Redis (together with Redisson) perfectly fits for this. By the way, we can replace the distributed locking with distributed semaphore without any difficulties.

## System Requirements
In order to launch the service you need to have the following tools: 

1. JDK 11 or higher to compile the service code (Already assembled artifact is placed build/libs/ folder).
2. Docker & docker-compose to deploy the entire system locally.
3. Bash interpreter to launch the deployment script (e.g. git bash for Windows).

## Deployment
Start a bash script 'run.sh' which is placed in the root folder of the project. This scripts makes the following steps:

1. Deploys the entire system in docker using docker-compose.
2. Waits for the Mongo to be ready.
3. Initializes Mongo replica set (it is necessary for transactions and Kafka connector)
4. Restart the service container.
5. Waits for the "Mongo - Kafka Connector" container to be ready.
6. Creates the Mongo - Kafka Connector to stream the data from 'messages' collection into Kafka.

If the entire system deployed successfully, you should see the message 'Query Performance Service started successfully' in console.

## Example
This example is based on the Postman Collection 'Query Performance.postman_collection.json' and runs a test against two environments: MySQL 8.0 and MySQL 5.7.

1. Create a new Environment for MySQL 8.0. Use 'Environment\Create Environment (mysql_8.0.18)';
2. Create a new Environment for MySQL 5.6. Use 'Environment\Create Environment (mysql_5.6.28)';
3. Create a scenario. Use 'Scenario\Create Scenario'. Copy 'id' from response payload.
4. Check the report for the Scenario. Use 'Scenario\Get Results by Scenario Id'. Paste the id in the url.