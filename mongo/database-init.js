query_performance = db.getSiblingDB("query_performance")

query_performance.createCollection("executions")
query_performance.createCollection("environments")
query_performance.createCollection("scenarios")
query_performance.createCollection("messages")

query_performance.createUser({
    user: "query_performance_admin",
    pwd: "query_performance_admin#pass",
    roles: [
        {
            role: "readWrite",
            db: "query_performance"
        }
    ]
})
