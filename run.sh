#!/bin/bash
docker-compose down
docker-compose up -d --build


printf '\n\nWaiting for the Mongo to be ready...'
function test_mongo_available {
  COUNTER=0
  while [[ -z "$(docker logs mongo | grep 'Did not find local replica set configuration')" ]]
  do
      let COUNTER+=1
      if [[ $COUNTER -gt 120 ]]; then
        echo 'Mongo is not in running state after 2 minutes.'
        exit 1
      fi
      sleep 1
      printf '.'
  done
}
test_mongo_available


printf '\nInitializing Mongo replica set\n'
docker exec mongo mongo -u root -p root-1-password --eval "if (rs.status()[\"ok\"] == 0) { rs.initiate({ _id: \"rs0\", members: [ { _id: 0, host: \"mongo:27017\", priority: 1.0 }]})}"


printf '\nRestarting query performance service\n'
docker restart query-performance-service


printf '\nWaiting for the "Mongo - Kafka Connector" container to be ready...'
function test_connector_available {
  COUNTER=0
  until $(curl --output /dev/null --silent --head --fail http://localhost:8083); do
      let COUNTER+=1
      if [[ $COUNTER -gt 240 ]]; then
        echo 'Mongo - Kafka Connector is not in running state after 4 minutes.'
        exit 1
      fi
      sleep 1
      printf '.'
  done
}
test_connector_available


printf '\nCreating the Mongo - Kafka Connector\n'
curl --output /dev/null --silent -X POST -H "Content-Type: application/json" --data '
  {
    "name": "messages-mongo-source",
    "config": {
        "tasks.max": "1",
        "connector.class": "com.mongodb.kafka.connect.MongoSourceConnector",
        "key.converter": "org.apache.kafka.connect.storage.StringConverter",
        "key.converter.schemas.enable": false,
        "value.converter": "org.apache.kafka.connect.storage.StringConverter",
        "value.converter.schemas.enable=false": false,
        "connection.uri": "mongodb://query_performance_admin:query_performance_admin#pass@mongo:27017/query_performance?ssl=false",
        "topic.prefix": "mongo",
        "database": "query_performance",
        "collection": "messages",
        "publish.full.document.only": true,
        "pipeline": "[{\"$match\": {\"operationType\": \"insert\"}}]"
    }
}' http://localhost:8083/connectors -w "\n"

printf '\nQuery Performance Service started successfully'