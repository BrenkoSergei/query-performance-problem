package by.branko.query.consumer;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.domain.ExecutionDto;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.service.EnvironmentService;
import by.branko.query.service.ExecutionService;
import by.branko.query.service.ScenarioService;
import by.branko.query.service.executor.ExecutionResult;
import by.branko.query.service.executor.QueryExecutor;
import by.branko.query.service.executor.QueryExecutorRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Kafka Listener for performance test execution.
 * Performs the following steps:
 * 1. Locks the environment usage using Redisson's RLock instance.
 * 2. Sets statuses of the Execution and corresponding Scenario to PROCESSING
 * 3. Searches for the Environment. If it does not exist, adds an error.
 * 4. If Environment exists, receives corresponding QueryExecutor. If it does not exist, adds an error.
 * 5. If Environment and QueryExecutor exist, executes the test and the results.
 * 6. Saves the Execution with results as FINISHED.
 * 7. If all the Scenario's Executions are in FINISHED status, sets FINISHED status for the Scenario.
 * 8. Releases lock.
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ExecutionConsumer {

    public static final String TOPIC_PREFIX = "query_execution_";
    private static final String LOCK_NAME_PREFIX = "query_execution_lock_";

    private final RedissonClient redissonClient;
    private final ScenarioService scenarioService;
    private final ExecutionService executionService;
    private final EnvironmentService environmentService;
    private final QueryExecutorRegistry queryExecutorRegistry;

    @KafkaListener(topicPattern = TOPIC_PREFIX + ".*")
    public void performExecution(@Payload ExecutionDto execution) {
        RLock lock = redissonClient.getLock(LOCK_NAME_PREFIX + execution.getEnvironmentName());
        int queryTimeout = execution.getQuery().getTimeoutInSeconds();
        int lockTimeout = (int) (1.1 * (execution.getWarmUpIterations() + execution.getMeasurements()) * queryTimeout);
        lock.lock(lockTimeout, TimeUnit.SECONDS);

        String query = execution.getQuery().getQuery();
        log.info("Performing test execution for environment {} and query {}", execution.getEnvironmentName(), query);
        scenarioService.updateStatus(execution.getScenarioId(), ExecutionStatus.PROCESSING);
        executionService.updateStatus(execution.getId(), ExecutionStatus.PROCESSING);

        environmentService.getById(execution.getEnvironmentId())
                .ifPresentOrElse(
                        environment -> doTest(execution, environment),
                        () -> execution.addError("Environment was not found.")
                );

        execution.setExecutionStatus(ExecutionStatus.FINISHED.name());
        executionService.save(execution);

        List<ExecutionDto> executions = executionService.getByScenarioId(execution.getScenarioId());
        boolean allExecutionsFinished = executions.stream()
                .map(ExecutionDto::getExecutionStatus)
                .allMatch(Predicate.isEqual(ExecutionStatus.FINISHED.name()));
        if (allExecutionsFinished) {
            scenarioService.updateStatus(execution.getScenarioId(), ExecutionStatus.FINISHED);
        }

        lock.unlock();
    }

    private void doTest(ExecutionDto execution, EnvironmentDto environment) {
        queryExecutorRegistry.getQueryExecutor(environment.getType())
                .ifPresentOrElse(
                        queryExecutor -> executeTest(queryExecutor, execution, environment),
                        () -> execution.addError("There is no appropriate query executor for type: " + environment.getType())
                );
    }

    private void executeTest(QueryExecutor queryExecutor, ExecutionDto execution, EnvironmentDto environment) {
        log.info("Executing warm up iterations for execution {}", execution.getId());
        Stream.generate(execution::getQuery)
                .limit(execution.getWarmUpIterations())
                .forEach(query -> queryExecutor.execute(environment, query));

        log.info("Executing measurements for execution {}", execution.getId());
        StopWatch stopWatch = new StopWatch();
        Stream.generate(execution::getQuery)
                .limit(execution.getMeasurements())
                .forEach(query -> {
                    stopWatch.start();
                    ExecutionResult executionResult = queryExecutor.execute(environment, query);
                    stopWatch.stop();

                    execution.addExecutionTimeMillis(stopWatch.getLastTaskTimeMillis());
                    execution.incrementExecutionCounter(executionResult.isSuccessful());
                    Optional.ofNullable(executionResult.getException())
                            .map(Throwable::getMessage)
                            .ifPresent(execution::addError);
                });
        log.info("Test execution finished for execution with id {}", execution.getId());
    }
}
