package by.branko.query.controller;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.service.EnvironmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/environments")
public class EnvironmentController {

    private final EnvironmentService environmentService;

    @GetMapping
    public List<EnvironmentDto> getAll() {
        return environmentService.getAll();
    }

    @GetMapping(params = "name")
    public List<EnvironmentDto> searchByName(@RequestParam("name") String name) {
        return environmentService.searchByName(name);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EnvironmentDto create(@Valid @RequestBody EnvironmentDto environment) {
        environment.setId(null);
        return environmentService.save(environment);
    }

    @PutMapping("/{id}")
    public EnvironmentDto update(@PathVariable String id,
                                 @Valid @RequestBody EnvironmentDto environment) {
        environment.setId(id);
        return environmentService.save(environment);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        environmentService.delete(id);
    }
}
