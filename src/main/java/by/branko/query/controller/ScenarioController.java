package by.branko.query.controller;

import by.branko.query.domain.ScenarioDto;
import by.branko.query.domain.ResultDto;
import by.branko.query.service.ScenarioService;
import by.branko.query.service.ReportService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scenarios")
public class ScenarioController {

    private final ReportService reportService;
    private final ScenarioService scenarioService;

    @GetMapping
    public List<ScenarioDto> getAll() {
        return scenarioService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<ScenarioDto> getById(@PathVariable String id) {
        return ResponseEntity.of(scenarioService.getById(id));
    }

    @GetMapping("/{id}/results")
    public List<ResultDto> getResultsById(@PathVariable String id,
                                          @RequestParam(value = "includeValues", defaultValue = "false") boolean includeValues) {
        return reportService.getExecutionResultByScenarioId(id, includeValues);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ScenarioDto submit(@Valid @RequestBody ScenarioDto executionScenario) {
        executionScenario.setId(null);
        return scenarioService.submit(executionScenario);
    }
}
