package by.branko.query.controller.exception;

import lombok.Getter;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Getter
public class ErrorResponse {

    private LocalDateTime timestamp = LocalDateTime.now();
    private List<String> errors;

    ErrorResponse(String error) {
        this.errors = Collections.singletonList(error);
    }

    ErrorResponse(List<String> errors) {
        this.errors = errors;
    }
}
