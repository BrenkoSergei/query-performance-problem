package by.branko.query.controller.exception;

import by.branko.query.service.exception.QueryPerformanceException;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ExceptionHandlingControllerAdvice {

    private static final String SUFFIX_MESSAGE = ".MESSAGE";
    private static final String SUFFIX_STATUS_CODE = ".STATUS_CODE";
    private static final int DEFAULT_STATUS_CODE = 500;

    private static final String CODE_HTTP_METHOD_NOT_SUPPORTED = "HTTP_METHOD_NOT_SUPPORTED";
    private static final String CODE_HTTP_MEDIA_TYPE_NOT_SUPPORTED = "HTTP_MEDIA_TYPE_NOT_SUPPORTED";

    private final MessageSource messageSource;

    private String getMessage(String code) {
        return getMessage(code, null);
    }

    private String getMessage(String code, Object[] params) {
        return messageSource.getMessage(code + SUFFIX_MESSAGE, params, Locale.getDefault());
    }

    private Integer getHttpStatusCode(String code) {
        return Try.of(() -> messageSource.getMessage(code + SUFFIX_STATUS_CODE, null, Locale.getDefault()))
                .map(Integer::parseInt)
                .getOrElse(DEFAULT_STATUS_CODE);
    }

    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ErrorResponse httpRequestMethodNotSupportedException(HttpMediaTypeNotSupportedException e) {
        log.error("A HttpMediaTypeNotSupportedException occurred.", e);
        String message = getMessage(CODE_HTTP_MEDIA_TYPE_NOT_SUPPORTED, null);
        return new ErrorResponse(message);
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ErrorResponse httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        log.error("A HttpRequestMethodNotSupportedException occurred.", e);
        String message = getMessage(CODE_HTTP_METHOD_NOT_SUPPORTED, null);
        return new ErrorResponse(message);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse methodArgumentNotValidException(MethodArgumentNotValidException e) {
        log.error("A validation error occurred before request processing.", e);
        List<String> messages = e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .map(this::getMessage)
                .collect(Collectors.toList());
        return new ErrorResponse(messages);
    }

    @ExceptionHandler(QueryPerformanceException.class)
    public ResponseEntity<ErrorResponse> queryPerformanceException(QueryPerformanceException e) {
        log.error("A query performance service exception occurred.", e);
        String errorCode = e.getErrorCode().name();
        return ResponseEntity.status(getHttpStatusCode(errorCode))
                .body(new ErrorResponse(getMessage(errorCode, e.getParams())));
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse exception(Exception e) {
        log.error("Unknown error occurred.", e);
        return new ErrorResponse(e.getMessage());
    }
}
