package by.branko.query.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

/**
 * Contains information about an Execution Environment (such as database installation)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = {"id", "creationDateTime", "lastUpdateDateTime"}, allowGetters = true)
public class EnvironmentDto {

    private String id;

    /**
     * Name (or tag) of the Environment. Must be unique.
     */
    @NotNull(message = "VALIDATION_ENVIRONMENT_NAME")
    @Pattern(regexp = "([a-z]|[0-9]|\\.|_)+", message = "VALIDATION_ENVIRONMENT_NAME")
    private String name;

    private String description;

    /**
     * The full instance url of the Environment. Must be unique for the entire system.
     */
    @NotBlank(message = "VALIDATION_ENVIRONMENT_INSTANCE_HOST")
    private String instanceUrl;

    /**
     * Properties of the connection to the Environment (e.g. username, password).
     */
    private Map<String, String> connectionProperties = Collections.emptyMap();

    /**
     * Query that will be used to check if the Environment is health.
     */
    @NotBlank(message = "VALIDATION_ENVIRONMENT_HEALTH_CHECK_QUERY")
    private String healthCheckQuery;

    /**
     * Type of the Environment. Specifies the relation to particular QueryExecutor.
     */
    private String type;

    private LocalDateTime creationDateTime;
    private LocalDateTime lastUpdateDateTime;
}
