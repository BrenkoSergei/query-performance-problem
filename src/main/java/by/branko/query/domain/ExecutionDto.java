package by.branko.query.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains information about one query execution against one environment. Stores some data from Environment and Scenario.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExecutionDto {

    private String id;
    private String scenarioId;
    private QueryDto query;
    private String environmentId;
    private String environmentName;
    private int warmUpIterations;
    private int measurements;

    private String executionStatus;
    private LocalDateTime submissionDateTime;
    private LocalDateTime lastActionDateTime;

    /**
     * List with execution results in milliseconds.
     */
    private List<Long> executionTimesMillis;

    /**
     * Count of successfully performed query executions. Should be less or equal to measurements values.
     */
    private int successExecutions;

    /**
     * Count of failed query executions. Should be less or equal to measurements values.
     */
    private int failingExecutions;

    /**
     * List of errors occurred during query executions.
     */
    private List<String> errors;

    public void addError(String error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        errors.add(error);
    }

    public void addExecutionTimeMillis(Long executionTimeMillis) {
        if (executionTimesMillis == null) {
            executionTimesMillis = new ArrayList<>();
        }
        executionTimesMillis.add(executionTimeMillis);
    }

    public void incrementExecutionCounter(boolean isSuccessful) {
        if (isSuccessful) {
            ++successExecutions;
        } else {
            ++failingExecutions;
        }
    }
}
