package by.branko.query.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.Map;

/**
 * Contains information about a query.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryDto {

    /**
     * Query string itself.
     */
    @NotBlank(message = "VALIDATION_QUERY_SHOULD_BE_PROVIDED")
    private String query;

    /**
     * Timeout for query execution in seconds.
     */
    private int timeoutInSeconds = 10;

    /**
     * Map of params that might be used for the query. E.g. params for named JDBC query.
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = Collections.emptyMap();
}
