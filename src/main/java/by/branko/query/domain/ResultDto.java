package by.branko.query.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Consolidation of the Executions from Scenario perspective. Stores results of Executions for the Scenario.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultDto {

    private QueryDto query;
    private String environmentName;
    private String executionStatus;
    private LocalDateTime submissionDateTime;
    private LocalDateTime lastActionDateTime;
    private int successExecutions;
    private int failingExecutions;
    private Set<String> errors;
    private List<Long> executionTimesMillis;

    private double min;
    private double max;
    private double mean;
    private double percentile50;
    private double percentile90;
    private double percentile95;
    private double percentile99;
}
