package by.branko.query.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

/**
 * Dto for test scenario.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(value = {"id", "executionStatus", "submissionDateTime", "lastActionDateTime"}, allowGetters = true)
public class ScenarioDto {

    private String id;

    /**
     * Description of the Scenario. It is used to provide human-friendly information.
     */
    private String description;

    /**
     * Queries to be run in scope of the test scenario.
     */
    @NotNull(message = "VALIDATION_QUERIES_SIZE")
    @Size(min = 1, max = 10, message = "VALIDATION_QUERIES_SIZE")
    private List<QueryDto> queries;

    /**
     * Names of the Environments to run queries against.
     */
    @NotNull(message = "VALIDATION_ENVIRONMENT_NAMES_SIZE")
    @Size(min = 1, max = 10, message = "VALIDATION_ENVIRONMENT_NAMES_SIZE")
    private List<String> environmentNames = Collections.emptyList();

    /**
     * Count of executions, that will be performed before the primary measurement.
     */
    @Min(value = 0, message = "VALIDATION_WARM_UP_ITERATIONS_MIN_MAX")
    @Max(value = 200, message = "VALIDATION_WARM_UP_ITERATIONS_MIN_MAX")
    private int warmUpIterations;

    /**
     * Count of measurements that will form the result statistics.
     */
    @Min(value = 1, message = "VALIDATION_MEASUREMENTS_MIN_MAX")
    @Max(value = 1000, message = "VALIDATION_MEASUREMENTS_MIN_MAX")
    private int measurements;

    /**
     * Current status of the execution.
     */
    private String executionStatus;

    private LocalDateTime submissionDateTime;
    private LocalDateTime lastActionDateTime;
}
