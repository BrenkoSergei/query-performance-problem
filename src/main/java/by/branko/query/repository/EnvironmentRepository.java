package by.branko.query.repository;

import by.branko.query.repository.entity.Environment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface EnvironmentRepository extends MongoRepository<Environment, String> {

    List<Environment> findByNameLike(String name);

    List<Environment> findByNameIn(Collection<String> names);
}
