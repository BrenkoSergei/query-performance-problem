package by.branko.query.repository;

import by.branko.query.repository.entity.Execution;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExecutionRepository extends MongoRepository<Execution, String> {

    List<Execution> findByScenarioId(String executionScenarioId);
}
