package by.branko.query.repository;

import by.branko.query.repository.entity.KafkaMessage;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface KafkaMessageRepository extends MongoRepository<KafkaMessage, String> {
}
