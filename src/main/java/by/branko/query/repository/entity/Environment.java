package by.branko.query.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document("environments")
public class Environment {

    @Id
    private String id;
    @Indexed(unique = true)
    private String name;
    private String description;
    @Indexed(unique = true)
    private String instanceUrl;
    private Map<String, String> connectionProperties;
    private String healthCheckQuery;
    private String type;
    @CreatedDate
    private LocalDateTime creationDateTime;
    @LastModifiedDate
    private LocalDateTime lastUpdateDateTime;
}
