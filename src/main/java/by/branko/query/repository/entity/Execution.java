package by.branko.query.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document("executions")
public class Execution {

    public static final String FIELD_EXECUTION_STATUS = "executionStatus";
    public static final String FIELD_LAST_ACTION_DATE_TIME = "lastActionDateTime";

    @Id
    private String id;
    @Indexed
    private String scenarioId;
    private Query query;
    private String environmentId;
    private String environmentName;
    private int warmUpIterations;
    private int measurements;
    @Field(FIELD_EXECUTION_STATUS)
    private ExecutionStatus executionStatus;
    @CreatedDate
    private LocalDateTime submissionDateTime;
    @LastModifiedDate
    @Field(FIELD_LAST_ACTION_DATE_TIME)
    private LocalDateTime lastActionDateTime;

    private List<Long> executionTimesMillis;
    private int successExecutions;
    private int failingExecutions;
    private List<String> errors;
}
