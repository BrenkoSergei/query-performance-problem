package by.branko.query.repository.entity;

public enum ExecutionStatus {

    PENDING,
    PROCESSING,
    FINISHED
}
