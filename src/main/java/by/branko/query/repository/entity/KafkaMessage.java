package by.branko.query.repository.entity;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document("messages")
public class KafkaMessage {

    @Id
    @JsonAlias("_id")
    private String id;
    private String key;
    private String topic;
    private String body;
}
