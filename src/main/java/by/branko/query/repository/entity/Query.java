package by.branko.query.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Query {

    private String query;
    private int timeoutInSeconds = 10;
    private Map<String, Object> params = Collections.emptyMap();
}
