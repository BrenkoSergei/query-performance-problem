package by.branko.query.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document("scenarios")
public class Scenario {

    public static final String FIELD_EXECUTION_STATUS = "executionStatus";
    public static final String FIELD_LAST_ACTION_DATE_TIME = "lastActionDateTime";

    @Id
    private String id;
    private String description;
    private List<Query> queries;
    private List<String> environmentNames;
    private int warmUpIterations;
    private int measurements;
    @Field(FIELD_EXECUTION_STATUS)
    private ExecutionStatus executionStatus;
    @CreatedDate
    private LocalDateTime submissionDateTime;
    @LastModifiedDate
    @Field(FIELD_LAST_ACTION_DATE_TIME)
    private LocalDateTime lastActionDateTime;
}
