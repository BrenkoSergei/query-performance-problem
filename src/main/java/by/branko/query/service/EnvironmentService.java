package by.branko.query.service;

import by.branko.query.domain.EnvironmentDto;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Service for operating with Environments.
 */
public interface EnvironmentService {

    /**
     * Retrieves all Environments
     */
    List<EnvironmentDto> getAll();

    /**
     * Retrieves Environments by id
     */
    Optional<EnvironmentDto> getById(String id);

    /**
     * Retrieves Environments by Collection of names
     */
    List<EnvironmentDto> getByNames(Collection<String> names);

    /**
     * Searches Environment by name
     */
    List<EnvironmentDto> searchByName(String name);

    /**
     * Saves Environment
     */
    EnvironmentDto save(EnvironmentDto environment);

    /**
     * Deletes Environment by id
     */
    void delete(String id);
}
