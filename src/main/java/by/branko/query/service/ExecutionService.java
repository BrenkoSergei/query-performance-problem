package by.branko.query.service;

import by.branko.query.domain.ExecutionDto;
import by.branko.query.repository.entity.ExecutionStatus;

import java.util.List;

/**
 * Service for operating with Executions
 */
public interface ExecutionService {

    /**
     * Retrieves List of Executions by Scenario id
     */
    List<ExecutionDto> getByScenarioId(String scenarioId);

    /**
     * Saves single Execution
     */
    ExecutionDto save(ExecutionDto execution);

    /**
     * Saves a bunch of Executions
     */
    List<ExecutionDto> saveAll(List<ExecutionDto> executions);

    /**
     * Updates Execution status by id
     */
    void updateStatus(String id, ExecutionStatus executionStatus);
}
