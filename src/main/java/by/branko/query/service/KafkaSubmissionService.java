package by.branko.query.service;

/**
 * Implements Transactional log tailing pattern by saving future Kafka messages
 * into a separate storage collection. After the saving, mongo's source kafka connector
 * tails transactions log and submits those messages into necessary Kafka topic.
 * <p>
 * By using this approach we can ensure that mongo transaction and submitting
 * into message broker will be performed atomically.
 */
public interface KafkaSubmissionService {

    /**
     * Submits message into kafka.
     */
    void submit(String topic, String key, Object body);
}
