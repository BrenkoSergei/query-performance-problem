package by.branko.query.service;

import by.branko.query.domain.ResultDto;

import java.util.List;

/**
 * Service for Reports
 */
public interface ReportService {

    /**
     * Generates 'report' by scenario id
     */
    List<ResultDto> getExecutionResultByScenarioId(String scenarioId, boolean includeValues);
}
