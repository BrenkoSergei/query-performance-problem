package by.branko.query.service;

import by.branko.query.domain.ScenarioDto;
import by.branko.query.repository.entity.ExecutionStatus;

import java.util.List;
import java.util.Optional;

/**
 * Service for operating with Scenarios
 */
public interface ScenarioService {

    /**
     * Retrieves all Scenarios
     */
    List<ScenarioDto> getAll();

    /**
     * Retrieves Scenario by id
     */
    Optional<ScenarioDto> getById(String id);

    /**
     * Submits new Scenario
     */
    ScenarioDto submit(ScenarioDto executionScenario);

    /**
     * Updates Scenario status by id
     */
    void updateStatus(String id, ExecutionStatus executionStatus);
}
