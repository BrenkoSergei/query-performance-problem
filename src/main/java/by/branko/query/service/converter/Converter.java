package by.branko.query.service.converter;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

public interface Converter<E, D> {

    D toDto(E entity);

    E toEntity(D dto);

    default List<D> toDtos(Collection<E> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(toList());
    }

    default List<E> toEntities(Collection<D> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(toList());
    }
}
