package by.branko.query.service.converter;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.repository.entity.Environment;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentConverter implements Converter<Environment, EnvironmentDto> {

    @Override
    public EnvironmentDto toDto(Environment entity) {
        return entity == null ? null : new EnvironmentDto(
                entity.getId(),
                entity.getName(),
                entity.getDescription(),
                entity.getInstanceUrl(),
                entity.getConnectionProperties(),
                entity.getHealthCheckQuery(),
                entity.getType(),
                entity.getCreationDateTime(),
                entity.getLastUpdateDateTime()
        );
    }

    @Override
    public Environment toEntity(EnvironmentDto dto) {
        return dto == null ? null : Environment.builder()
                .id(dto.getId())
                .name(dto.getName())
                .description(dto.getDescription())
                .instanceUrl(dto.getInstanceUrl())
                .connectionProperties(dto.getConnectionProperties())
                .healthCheckQuery(dto.getHealthCheckQuery())
                .type(dto.getType())
                .build();
    }
}
