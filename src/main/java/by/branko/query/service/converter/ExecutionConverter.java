package by.branko.query.service.converter;

import by.branko.query.domain.ExecutionDto;
import by.branko.query.domain.QueryDto;
import by.branko.query.repository.entity.Execution;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.repository.entity.Query;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ExecutionConverter implements Converter<Execution, ExecutionDto> {

    private final Converter<Query, QueryDto> queryConverter;

    @Override
    public ExecutionDto toDto(Execution entity) {
        return entity == null ? null : ExecutionDto.builder()
                .id(entity.getId())
                .scenarioId(entity.getScenarioId())
                .environmentId(entity.getEnvironmentId())
                .environmentName(entity.getEnvironmentName())
                .query(queryConverter.toDto(entity.getQuery()))
                .warmUpIterations(entity.getWarmUpIterations())
                .measurements(entity.getMeasurements())
                .executionStatus(entity.getExecutionStatus().name())
                .submissionDateTime(entity.getSubmissionDateTime())
                .lastActionDateTime(entity.getLastActionDateTime())
                .executionTimesMillis(entity.getExecutionTimesMillis())
                .successExecutions(entity.getSuccessExecutions())
                .failingExecutions(entity.getFailingExecutions())
                .errors(entity.getErrors())
                .build();
    }

    @Override
    public Execution toEntity(ExecutionDto dto) {
        return dto == null ? null : Execution.builder()
                .id(dto.getId())
                .scenarioId(dto.getScenarioId())
                .environmentId(dto.getEnvironmentId())
                .environmentName(dto.getEnvironmentName())
                .query(queryConverter.toEntity(dto.getQuery()))
                .warmUpIterations(dto.getWarmUpIterations())
                .measurements(dto.getMeasurements())
                .executionStatus(ExecutionStatus.valueOf(dto.getExecutionStatus()))
                .submissionDateTime(dto.getSubmissionDateTime())
                .lastActionDateTime(dto.getLastActionDateTime())
                .executionTimesMillis(dto.getExecutionTimesMillis())
                .successExecutions(dto.getSuccessExecutions())
                .failingExecutions(dto.getFailingExecutions())
                .errors(dto.getErrors())
                .build();
    }
}
