package by.branko.query.service.converter;

import by.branko.query.domain.ScenarioDto;
import by.branko.query.domain.QueryDto;
import by.branko.query.repository.entity.Scenario;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.repository.entity.Query;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ExecutionScenarioConverter implements Converter<Scenario, ScenarioDto> {

    private final Converter<Query, QueryDto> queryConverter;

    @Override
    public ScenarioDto toDto(Scenario entity) {
        return entity == null ? null : new ScenarioDto(
                entity.getId(),
                entity.getDescription(),
                queryConverter.toDtos(entity.getQueries()),
                entity.getEnvironmentNames(),
                entity.getWarmUpIterations(),
                entity.getMeasurements(),
                entity.getExecutionStatus().name(),
                entity.getSubmissionDateTime(),
                entity.getLastActionDateTime()
        );
    }

    @Override
    public Scenario toEntity(ScenarioDto dto) {
        return dto == null ? null : Scenario.builder()
                .id(dto.getId())
                .description(dto.getDescription())
                .queries(queryConverter.toEntities(dto.getQueries()))
                .environmentNames(dto.getEnvironmentNames())
                .warmUpIterations(dto.getWarmUpIterations())
                .measurements(dto.getMeasurements())
                .executionStatus(Optional.ofNullable(dto.getExecutionStatus()).map(ExecutionStatus::valueOf).orElse(null))
                .submissionDateTime(dto.getSubmissionDateTime())
                .lastActionDateTime(dto.getLastActionDateTime())
                .build();
    }
}
