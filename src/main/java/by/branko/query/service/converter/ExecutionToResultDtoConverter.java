package by.branko.query.service.converter;

import by.branko.query.domain.QueryDto;
import by.branko.query.domain.ResultDto;
import by.branko.query.repository.entity.Execution;
import by.branko.query.repository.entity.Query;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ExecutionToResultDtoConverter implements Converter<Execution, ResultDto> {

    private final Converter<Query, QueryDto> queryConverter;

    @Override
    public ResultDto toDto(Execution entity) {
        return entity == null ? null : ResultDto.builder()
                .query(queryConverter.toDto(entity.getQuery()))
                .environmentName(entity.getEnvironmentName())
                .executionStatus(entity.getExecutionStatus().name())
                .submissionDateTime(entity.getSubmissionDateTime())
                .lastActionDateTime(entity.getLastActionDateTime())
                .successExecutions(entity.getSuccessExecutions())
                .failingExecutions(entity.getFailingExecutions())
                .errors(Optional.ofNullable(entity.getErrors()).map(HashSet::new).orElse(null))
                .executionTimesMillis(entity.getExecutionTimesMillis())
                .build();
    }

    @Override
    public Execution toEntity(ResultDto dto) {
        throw new UnsupportedOperationException();
    }
}
