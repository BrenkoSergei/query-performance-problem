package by.branko.query.service.converter;

import by.branko.query.domain.QueryDto;
import by.branko.query.repository.entity.Query;
import org.springframework.stereotype.Component;

@Component
public class QueryConverter implements Converter<Query, QueryDto> {

    @Override
    public QueryDto toDto(Query entity) {
        return entity == null ? null : new QueryDto(
                entity.getQuery(),
                entity.getTimeoutInSeconds(),
                entity.getParams()
        );
    }

    @Override
    public Query toEntity(QueryDto dto) {
        return dto == null ? null : new Query(
                dto.getQuery(),
                dto.getTimeoutInSeconds(),
                dto.getParams()
        );
    }
}
