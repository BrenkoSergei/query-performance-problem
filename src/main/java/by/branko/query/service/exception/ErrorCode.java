package by.branko.query.service.exception;

public enum ErrorCode {

    ENVIRONMENT_NOT_FOUND,
    ENVIRONMENT_NAME_OR_INSTANCE_ALREADY_IN_USE,
    ENVIRONMENT_TYPE_NOT_SUPPORTED,
    ENVIRONMENT_NOT_HEALTH,

    SCENARIO_NOT_FOUND,
    SCENARIO_ENVIRONMENTS_NOT_FOUND,

    EXECUTION_NOT_FOUND
}
