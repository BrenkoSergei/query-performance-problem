package by.branko.query.service.exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class QueryPerformanceException extends RuntimeException {

    private final ErrorCode errorCode;
    private Object[] params;

    public QueryPerformanceException(ErrorCode errorCode, Object... params) {
        this.errorCode = errorCode;
        this.params = params;
    }

    @Override
    public String getMessage() {
        return errorCode.name();
    }
}
