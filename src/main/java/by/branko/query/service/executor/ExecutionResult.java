package by.branko.query.service.executor;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ExecutionResult {

    private final boolean successful;
    private final Exception exception;

    public static ExecutionResult successful() {
        return new ExecutionResult(true, null);
    }

    public static ExecutionResult failing(Exception e) {
        return new ExecutionResult(false, e);
    }
}
