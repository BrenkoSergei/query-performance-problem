package by.branko.query.service.executor;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.domain.QueryDto;

import java.util.Set;

public interface QueryExecutor {

    ExecutionResult execute(EnvironmentDto environment, QueryDto query);

    boolean isHealth(EnvironmentDto environment);

    Set<String> getSupportedTypes();
}
