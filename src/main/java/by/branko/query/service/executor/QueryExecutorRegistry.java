package by.branko.query.service.executor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Component
public class QueryExecutorRegistry {

    private final Map<String, QueryExecutor> registry;

    public QueryExecutorRegistry(List<QueryExecutor> queryExecutors) {
        Map<String, QueryExecutor> typeToQueryExecutor = new HashMap<>();

        queryExecutors.forEach(queryExecutor ->
                queryExecutor.getSupportedTypes().forEach(type -> {
                    if (typeToQueryExecutor.put(type, queryExecutor) != null) {
                        throw new RuntimeException("Duplicate supported type for QueryExecutor: " + type);
                    }
                })
        );

        registry = Collections.unmodifiableMap(typeToQueryExecutor);
    }

    public Set<String> getSupportedTypes() {
        return registry.keySet();
    }

    public Optional<QueryExecutor> getQueryExecutor(String type) {
        log.info("Retrieving QueryExecutor by type {}", type);
        return Optional.ofNullable(registry.get(type));
    }
}
