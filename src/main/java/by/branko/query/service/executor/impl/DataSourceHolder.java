package by.branko.query.service.executor.impl;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class DataSourceHolder {

    private final Cache<String, DataSourceSpec> cache;

    public DataSourceHolder() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(20)
                .expireAfterAccess(60, TimeUnit.MINUTES)
                .removalListener(notification -> ((DataSourceSpec) notification.getValue()).dataSource.close())
                .build();
    }

    DataSource getDataSource(String instanceUrl, Map<String, String> connectionProperties) {
        DataSourceSpec dataSourceSpec = cache.getIfPresent(instanceUrl);

        if (dataSourceSpec == null || !dataSourceSpec.connectionProperties.equals(connectionProperties)) {
            HikariDataSource dataSource = createDataSource(instanceUrl, connectionProperties);
            dataSourceSpec = new DataSourceSpec(connectionProperties, dataSource);
            cache.put(instanceUrl, dataSourceSpec);
        }

        return dataSourceSpec.dataSource;
    }

    private HikariDataSource createDataSource(String instanceUrl, Map<String, String> connectionProperties) {
        log.info("Creating new data source for instance url {}", instanceUrl);
        try {
            DriverManager.getDriver(instanceUrl);
        } catch (SQLException e) {
            throw new RuntimeException("JDBC Driver for the instance url cannot be found.");
        }

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(instanceUrl);
        hikariConfig.setMaximumPoolSize(1);

        Properties properties = new Properties();
        properties.putAll(connectionProperties);
        hikariConfig.setDataSourceProperties(properties);

        return new HikariDataSource(hikariConfig);
    }

    @AllArgsConstructor
    private static class DataSourceSpec {
        private Map<String, String> connectionProperties;
        private HikariDataSource dataSource;
    }
}
