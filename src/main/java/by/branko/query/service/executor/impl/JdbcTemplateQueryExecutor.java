package by.branko.query.service.executor.impl;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.domain.QueryDto;
import by.branko.query.service.executor.ExecutionResult;
import by.branko.query.service.executor.QueryExecutor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class JdbcTemplateQueryExecutor implements QueryExecutor {

    private static final Set<String> SUPPORTED_TYPES = Collections.singleton("jdbc");

    private final DataSourceHolder dataSourceHolder;

    @Override
    public ExecutionResult execute(EnvironmentDto environment, QueryDto query) {
        JdbcTemplate jdbcTemplate = resolveJdbcTemplate(environment);
        jdbcTemplate.setQueryTimeout(query.getTimeoutInSeconds());
        try {
            jdbcTemplate.execute(query.getQuery());
            return ExecutionResult.successful();
        } catch (Exception e) {
            return ExecutionResult.failing(e);
        }
    }

    @Override
    public boolean isHealth(EnvironmentDto environment) {
        log.info("Performing health check for environment {}", environment.getName());
        JdbcTemplate jdbcTemplate = resolveJdbcTemplate(environment);
        try {
            jdbcTemplate.execute(environment.getHealthCheckQuery());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private JdbcTemplate resolveJdbcTemplate(EnvironmentDto environment) {
        DataSource dataSource = dataSourceHolder.getDataSource(environment.getInstanceUrl(), environment.getConnectionProperties());
        return new JdbcTemplate(dataSource);
    }

    @Override
    public Set<String> getSupportedTypes() {
        return SUPPORTED_TYPES;
    }
}
