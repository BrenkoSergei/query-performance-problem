package by.branko.query.service.impl;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.repository.EnvironmentRepository;
import by.branko.query.repository.entity.Environment;
import by.branko.query.service.EnvironmentService;
import by.branko.query.service.converter.Converter;
import by.branko.query.service.exception.ErrorCode;
import by.branko.query.service.exception.QueryPerformanceException;
import by.branko.query.service.executor.QueryExecutor;
import by.branko.query.service.executor.QueryExecutorRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class EnvironmentServiceImpl implements EnvironmentService {

    private final QueryExecutorRegistry queryExecutorRegistry;
    private final EnvironmentRepository environmentRepository;
    private final Converter<Environment, EnvironmentDto> environmentConverter;

    @Override
    public List<EnvironmentDto> getAll() {
        log.info("Loading all environments.");
        List<Environment> environments = environmentRepository.findAll();
        log.info("Count of loaded environments: {}", environments.size());
        return environmentConverter.toDtos(environments);
    }

    @Override
    public Optional<EnvironmentDto> getById(String id) {
        log.info("Loading environment by id {}", id);
        return environmentRepository.findById(id)
                .map(environmentConverter::toDto);
    }

    @Override
    public List<EnvironmentDto> getByNames(Collection<String> names) {
        log.info("Loading environments by Collection of names: {}", names);
        List<Environment> environments = environmentRepository.findByNameIn(names);
        log.info("Count of loaded environments: {}", environments.size());
        return environmentConverter.toDtos(environments);
    }

    @Override
    public List<EnvironmentDto> searchByName(String name) {
        log.info("Searching environments by name {}", name);
        List<Environment> environments = environmentRepository.findByNameLike(name);
        log.info("Count of found environments: {}", environments.size());
        return environmentConverter.toDtos(environments);
    }

    @Override
    @Transactional
    public EnvironmentDto save(EnvironmentDto environmentDto) {
        log.info("Saving environment: {}", environmentDto);
        Environment environment = environmentConverter.toEntity(environmentDto);

        if (environment.getId() != null && !environmentRepository.existsById(environment.getId())) {
            log.error("Environment with id {} does not exist", environment.getId());
            throw new QueryPerformanceException(ErrorCode.ENVIRONMENT_NOT_FOUND, environment.getId());
        }
        validateConstraints(environment);
        validateHeath(environmentDto);

        log.info("Saving environment in database");
        environment = environmentRepository.save(environment);
        return environmentConverter.toDto(environment);
    }

    private void validateConstraints(Environment environment) {
        log.info("Validating constraints for saving environment");
        Environment probe = Environment.builder()
                .name(environment.getName())
                .instanceUrl(environment.getInstanceUrl())
                .build();

        log.info("Loading environments with name {} or instance url {}", environment.getName(), environment.getInstanceUrl());
        List<Environment> existingEnvironments = environmentRepository.findAll(Example.of(probe, ExampleMatcher.matchingAny()));
        log.info("Count of loaded environments: {}", existingEnvironments.size());

        existingEnvironments.stream()
                .filter(env -> environment.getId() == null || !env.getId().equals(environment.getId()))
                .findAny()
                .ifPresent(env -> {
                    throw new QueryPerformanceException(ErrorCode.ENVIRONMENT_NAME_OR_INSTANCE_ALREADY_IN_USE);
                });
    }

    private void validateHeath(EnvironmentDto environment) {
        QueryExecutor queryExecutor = queryExecutorRegistry.getQueryExecutor(environment.getType())
                .orElseThrow(() ->
                        new QueryPerformanceException(ErrorCode.ENVIRONMENT_TYPE_NOT_SUPPORTED, queryExecutorRegistry.getSupportedTypes())
                );
        log.info("Validating if saving environment is health");
        if (!queryExecutor.isHealth(environment)) {
            throw new QueryPerformanceException(ErrorCode.ENVIRONMENT_NOT_HEALTH);
        }
    }

    @Override
    public void delete(String id) {
        log.info("Checking if environment with id {} exists", id);
        if (environmentRepository.existsById(id)) {
            log.info("Deleting environment by id {}", id);
            environmentRepository.deleteById(id);
        } else {
            throw new QueryPerformanceException(ErrorCode.ENVIRONMENT_NOT_FOUND, id);
        }
    }
}
