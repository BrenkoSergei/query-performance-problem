package by.branko.query.service.impl;

import by.branko.query.domain.ExecutionDto;
import by.branko.query.repository.ExecutionRepository;
import by.branko.query.repository.entity.Execution;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.service.ExecutionService;
import by.branko.query.service.converter.Converter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExecutionServiceImpl implements ExecutionService {

    private final MongoOperations mongoOperations;
    private final ExecutionRepository executionRepository;
    private final Converter<Execution, ExecutionDto> executionConverter;

    @Override
    public List<ExecutionDto> getByScenarioId(String scenarioId) {
        log.info("Loading scenario by id {}", scenarioId);
        List<Execution> executions = executionRepository.findByScenarioId(scenarioId);
        log.info("Count of loaded scenarios: {}", executions.size());
        return executionConverter.toDtos(executions);
    }

    @Override
    public ExecutionDto save(ExecutionDto executionDto) {
        log.info("Saving execution: {}", executionDto);
        Execution execution = executionConverter.toEntity(executionDto);
        execution = executionRepository.save(execution);
        return executionConverter.toDto(execution);
    }

    @Override
    public List<ExecutionDto> saveAll(List<ExecutionDto> executionDtos) {
        log.info("Saving bunch of executions. Count: {}", executionDtos.size());
        List<Execution> executions = executionConverter.toEntities(executionDtos);
        executions = executionRepository.saveAll(executions);
        return executionConverter.toDtos(executions);
    }

    @Override
    public void updateStatus(String id, ExecutionStatus executionStatus) {
        log.info("Updating status for execution with id {} onto {}", id, executionStatus);
        mongoOperations.updateFirst(
                new Query(Criteria.where("_id").is(id)),
                new Update().set(Execution.FIELD_EXECUTION_STATUS, executionStatus)
                        .set(Execution.FIELD_LAST_ACTION_DATE_TIME, LocalDateTime.now()),
                Execution.class
        );
    }
}
