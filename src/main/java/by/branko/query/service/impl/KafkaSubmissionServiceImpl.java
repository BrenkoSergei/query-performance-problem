package by.branko.query.service.impl;

import by.branko.query.repository.KafkaMessageRepository;
import by.branko.query.repository.entity.KafkaMessage;
import by.branko.query.service.KafkaSubmissionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaSubmissionServiceImpl implements KafkaSubmissionService {

    private final KafkaMessageRepository kafkaMessageRepository;
    private final KafkaTemplate<String, Object> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @Override
    public void submit(String topic, String key, Object body) {
        log.info("Saving message for the following submission into kafka topic {}", topic);
        String serializedBody = Try.of(() -> objectMapper.writeValueAsString(body))
                .getOrElseThrow(() -> new RuntimeException("kafka"));
        KafkaMessage kafkaMessage = new KafkaMessage(UUID.randomUUID().toString(), key, topic, serializedBody);
        kafkaMessageRepository.save(kafkaMessage);
    }

    @KafkaListener(topics = "mongo.query_performance.messages", groupId = "relay")
    public void relay(@Payload KafkaMessage kafkaMessage) {
        log.info("Relaying kafka message into topic {}", kafkaMessage.getTopic());
        String key = Optional.ofNullable(kafkaMessage.getKey())
                .orElseGet(() -> UUID.randomUUID().toString());
        Map body = Try.of(() -> objectMapper.readValue(kafkaMessage.getBody(), Map.class))
                .getOrElseThrow(() -> new RuntimeException("Kafka"));
        kafkaTemplate.send(kafkaMessage.getTopic(), key, body);
    }

    @KafkaListener(topics = "mongo.query_performance.messages", groupId = "cleaner")
    public void clean(@Payload KafkaMessage kafkaMessage) {
        log.info("Deleting already submitted message from permanent storage.");
        kafkaMessageRepository.deleteById(kafkaMessage.getId());
    }
}
