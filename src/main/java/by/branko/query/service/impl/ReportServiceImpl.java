package by.branko.query.service.impl;

import by.branko.query.domain.ResultDto;
import by.branko.query.repository.ExecutionRepository;
import by.branko.query.repository.entity.Execution;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.service.ReportService;
import by.branko.query.service.converter.Converter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final ExecutionRepository executionRepository;
    private final Converter<Execution, ResultDto> executionToResultDtoConverter;

    @Override
    public List<ResultDto> getExecutionResultByScenarioId(String scenarioId, boolean includeValues) {
        log.info("Generating report for scenario {}", scenarioId);
        List<Execution> executions = executionRepository.findByScenarioId(scenarioId);
        log.info("Count of loaded executions: {}", executions.size());
        List<ResultDto> results = executionToResultDtoConverter.toDtos(executions);

        results.stream()
                .filter(result -> result.getExecutionStatus().equals(ExecutionStatus.FINISHED.name()))
                .forEach(this::calculateMetrics);
        if (!includeValues) {
            log.info("Erasing values from report for scenario with id {}", scenarioId);
            results.forEach(result -> result.setExecutionTimesMillis(null));
        }

        return results;
    }

    private void calculateMetrics(ResultDto result) {
        DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
        result.getExecutionTimesMillis()
                .forEach(descriptiveStatistics::addValue);

        result.setMin(descriptiveStatistics.getMin());
        result.setMax(descriptiveStatistics.getMax());
        result.setMean(descriptiveStatistics.getMean());
        result.setPercentile50(descriptiveStatistics.getPercentile(50));
        result.setPercentile90(descriptiveStatistics.getPercentile(90));
        result.setPercentile95(descriptiveStatistics.getPercentile(95));
        result.setPercentile99(descriptiveStatistics.getPercentile(99));
    }
}
