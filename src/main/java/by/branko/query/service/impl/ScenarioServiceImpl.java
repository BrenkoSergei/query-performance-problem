package by.branko.query.service.impl;

import by.branko.query.consumer.ExecutionConsumer;
import by.branko.query.domain.EnvironmentDto;
import by.branko.query.domain.ExecutionDto;
import by.branko.query.domain.ScenarioDto;
import by.branko.query.repository.ScenarioRepository;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.repository.entity.Scenario;
import by.branko.query.service.EnvironmentService;
import by.branko.query.service.ExecutionService;
import by.branko.query.service.KafkaSubmissionService;
import by.branko.query.service.ScenarioService;
import by.branko.query.service.converter.Converter;
import by.branko.query.service.exception.ErrorCode;
import by.branko.query.service.exception.QueryPerformanceException;
import by.branko.query.service.executor.QueryExecutorRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScenarioServiceImpl implements ScenarioService {

    private final MongoOperations mongoOperations;
    private final ScenarioRepository scenarioRepository;
    private final ExecutionService executionService;
    private final EnvironmentService environmentService;
    private final QueryExecutorRegistry queryExecutorRegistry;
    private final KafkaSubmissionService kafkaSubmissionService;
    private final Converter<Scenario, ScenarioDto> executionScenarioConverter;

    @Override
    public List<ScenarioDto> getAll() {
        log.info("Loading all scenarios.");
        List<Scenario> scenarios = scenarioRepository.findAll();
        log.info("Count of loaded scenarios: {}", scenarios.size());
        return executionScenarioConverter.toDtos(scenarios);
    }

    @Override
    public Optional<ScenarioDto> getById(String id) {
        log.info("Loading scenario by id {}", id);
        return scenarioRepository.findById(id)
                .map(executionScenarioConverter::toDto);
    }

    @Override
    @Transactional
    public ScenarioDto submit(ScenarioDto scenarioDto) {
        log.info("Submitting new scenario: {}", scenarioDto);
        List<EnvironmentDto> environments = resolveEnvironments(scenarioDto);
        Map<String, String> environmentNameToId = environments.stream()
                .collect(Collectors.toMap(EnvironmentDto::getName, EnvironmentDto::getId));

        Scenario scenario = executionScenarioConverter.toEntity(scenarioDto);
        scenario.setExecutionStatus(ExecutionStatus.PENDING);

        log.info("Saving new scenario: {}", scenario);
        scenario = scenarioRepository.save(scenario);
        scenarioDto = executionScenarioConverter.toDto(scenario);

        List<ExecutionDto> executions = splitOntoExecutions(scenarioDto, environmentNameToId);
        log.info("Saving executions. Count: {}", executions.size());
        executions = executionService.saveAll(executions);
        executions.forEach(execution ->
                kafkaSubmissionService.submit(getTopicName(execution.getEnvironmentName()), execution.getId(), execution)
        );

        return scenarioDto;
    }

    private String getTopicName(String environmentName) {
        return ExecutionConsumer.TOPIC_PREFIX + environmentName;
    }

    private List<EnvironmentDto> resolveEnvironments(ScenarioDto scenarioDto) {
        Set<String> environmentNames = new HashSet<>(scenarioDto.getEnvironmentNames());
        List<EnvironmentDto> environments = environmentService.getByNames(environmentNames);
        environments.forEach(environment -> environmentNames.remove(environment.getName()));

        if (!environmentNames.isEmpty()) {
            throw new QueryPerformanceException(ErrorCode.SCENARIO_ENVIRONMENTS_NOT_FOUND, environmentNames);
        }

        environments.forEach(environment ->
                queryExecutorRegistry.getQueryExecutor(environment.getType())
                        .filter(queryExecutor -> !queryExecutor.isHealth(environment))
                        .ifPresent(queryExecutor -> {
                            throw new QueryPerformanceException(ErrorCode.ENVIRONMENT_NOT_HEALTH);
                        })
        );
        return environments;
    }

    private List<ExecutionDto> splitOntoExecutions(ScenarioDto executionScenario, Map<String, String> environmentNameToId) {
        ExecutionDto.ExecutionDtoBuilder builder = ExecutionDto.builder()
                .scenarioId(executionScenario.getId())
                .warmUpIterations(executionScenario.getWarmUpIterations())
                .measurements(executionScenario.getMeasurements())
                .executionStatus(ExecutionStatus.PENDING.name());

        return executionScenario.getEnvironmentNames()
                .stream()
                .peek(builder::environmentName)
                .peek(environmentName -> builder.environmentId(environmentNameToId.get(environmentName)))
                .flatMap(environmentName -> executionScenario.getQueries()
                        .stream()
                        .map(builder::query)
                        .map(ExecutionDto.ExecutionDtoBuilder::build)
                )
                .collect(Collectors.toList());
    }

    @Override
    public void updateStatus(String id, ExecutionStatus executionStatus) {
        log.info("Updating status for execution with id {} onto {}", id, executionStatus);
        mongoOperations.updateFirst(
                new Query(Criteria.where("_id").is(id)),
                new Update().set(Scenario.FIELD_EXECUTION_STATUS, executionStatus)
                        .set(Scenario.FIELD_LAST_ACTION_DATE_TIME, LocalDateTime.now()),
                Scenario.class
        );
    }
}
