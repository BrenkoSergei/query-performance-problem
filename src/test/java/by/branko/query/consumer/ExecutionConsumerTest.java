package by.branko.query.consumer;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.domain.ExecutionDto;
import by.branko.query.domain.QueryDto;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.service.EnvironmentService;
import by.branko.query.service.ExecutionService;
import by.branko.query.service.ScenarioService;
import by.branko.query.service.executor.ExecutionResult;
import by.branko.query.service.executor.QueryExecutor;
import by.branko.query.service.executor.QueryExecutorRegistry;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ExecutionConsumerTest {

    private static final String QUERY_QUERY = RandomStringUtils.randomAlphabetic(10);
    private static final int QUERY_TIMEOUT_IN_SECONDS = 10;

    private static final String ENVIRONMENT_ID = UUID.randomUUID().toString();
    private static final String ENVIRONMENT_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String ENVIRONMENT_DESCRIPTION = RandomStringUtils.randomAlphabetic(10);
    private static final String ENVIRONMENT_INSTANCE_URL = RandomStringUtils.randomAlphabetic(10);
    private static final Map<String, String> ENVIRONMENT_CONNECTION_PROPERTIES = Collections.emptyMap();
    private static final String ENVIRONMENT_HEALTH_CHECK_QUERY = RandomStringUtils.randomAlphabetic(10);
    private static final String ENVIRONMENT_TYPE = RandomStringUtils.randomAlphabetic(10);
    private static final LocalDateTime ENVIRONMENT_CREATION_DATE_TIME = LocalDateTime.now();
    private static final LocalDateTime ENVIRONMENT_LAST_UPDATE_DATE_TIME = LocalDateTime.now();

    private static final String EXECUTION_ID = UUID.randomUUID().toString();
    private static final String EXECUTION_SCENARIO_ID = UUID.randomUUID().toString();
    private static final String EXECUTION_ENVIRONMENT_ID = RandomStringUtils.randomAlphabetic(10);
    private static final String EXECUTION_ENVIRONMENT_NAME = RandomStringUtils.randomAlphabetic(10);
    private static final int EXECUTION_WARM_UP_ITERATIONS = 10;
    private static final int EXECUTION_MEASUREMENTS = 10;
    private static final String EXECUTION_EXECUTION_STATUS = ExecutionStatus.PENDING.name();

    @InjectMocks
    private ExecutionConsumer executionConsumer;

    @Mock
    private RedissonClient redissonClient;
    @Mock
    private RLock rLock;
    @Mock
    private ScenarioService scenarioService;
    @Mock
    private ExecutionService executionService;
    @Mock
    private EnvironmentService environmentService;
    @Mock
    private QueryExecutorRegistry queryExecutorRegistry;
    @Mock
    private QueryExecutor queryExecutor;

    @Test
    void performExecution_CompletesSuccessfully_WhenNoErrorsOccurred() {
        EnvironmentDto environment = getEnvironmentDto();
        ExecutionDto execution = getExecutionDto();
        QueryDto query = execution.getQuery();

        when(redissonClient.getLock(anyString())).thenReturn(rLock);
        when(environmentService.getById(EXECUTION_ENVIRONMENT_ID)).thenReturn(Optional.of(environment));
        when(executionService.save(eq(execution))).thenReturn(execution);
        when(executionService.getByScenarioId(eq(EXECUTION_SCENARIO_ID))).thenReturn(List.of(execution));
        when(queryExecutorRegistry.getQueryExecutor(eq(ENVIRONMENT_TYPE))).thenReturn(Optional.of(queryExecutor));
        when(queryExecutor.execute(eq(environment), eq(query))).thenReturn(ExecutionResult.successful());

        executionConsumer.performExecution(execution);

        assertNotNull(execution.getExecutionTimesMillis());
        assertEquals(execution.getMeasurements(), execution.getSuccessExecutions());
        assertEquals(execution.getSuccessExecutions(), execution.getExecutionTimesMillis().size());
        assertEquals(execution.getExecutionStatus(), ExecutionStatus.FINISHED.name());
        assertNull(execution.getErrors());
        assertEquals(0, execution.getFailingExecutions());

        verify(redissonClient, times(1)).getLock(anyString());
        verify(rLock, times(1)).lock(anyLong(), any());
        verify(rLock, times(1)).unlock();
        verify(scenarioService, times(1)).updateStatus(eq(EXECUTION_SCENARIO_ID), eq(ExecutionStatus.PROCESSING));
        verify(scenarioService, times(1)).updateStatus(eq(EXECUTION_SCENARIO_ID), eq(ExecutionStatus.FINISHED));
        verify(executionService, times(1)).updateStatus(eq(EXECUTION_ID), eq(ExecutionStatus.PROCESSING));
        verify(environmentService, times(1)).getById(eq(EXECUTION_ENVIRONMENT_ID));
        verify(executionService, times(1)).save(any());
        verify(executionService, times(1)).getByScenarioId(eq(EXECUTION_SCENARIO_ID));
        verify(queryExecutorRegistry, times(1)).getQueryExecutor(eq(ENVIRONMENT_TYPE));
        verify(queryExecutor, times(execution.getWarmUpIterations() + execution.getMeasurements())).execute(any(), any());
    }

    @Test
    void performExecution_ShouldNotExecuteTest_WhenQueryExecutorNotFound() {
        EnvironmentDto environment = getEnvironmentDto();
        ExecutionDto execution = getExecutionDto();

        when(redissonClient.getLock(anyString())).thenReturn(rLock);
        when(environmentService.getById(EXECUTION_ENVIRONMENT_ID)).thenReturn(Optional.of(environment));
        when(executionService.save(eq(execution))).thenReturn(execution);
        when(executionService.getByScenarioId(eq(EXECUTION_SCENARIO_ID))).thenReturn(List.of(execution));
        when(queryExecutorRegistry.getQueryExecutor(eq(ENVIRONMENT_TYPE))).thenReturn(Optional.empty());

        executionConsumer.performExecution(execution);

        assertNull(execution.getExecutionTimesMillis());
        assertEquals(0, execution.getSuccessExecutions());
        assertEquals(0, execution.getFailingExecutions());
        assertEquals(execution.getExecutionStatus(), ExecutionStatus.FINISHED.name());
        assertNotNull(execution.getErrors());
        assertEquals(1, execution.getErrors().size());

        verify(redissonClient, times(1)).getLock(anyString());
        verify(rLock, times(1)).lock(anyLong(), any());
        verify(rLock, times(1)).unlock();
        verify(scenarioService, times(1)).updateStatus(eq(EXECUTION_SCENARIO_ID), eq(ExecutionStatus.PROCESSING));
        verify(scenarioService, times(1)).updateStatus(eq(EXECUTION_SCENARIO_ID), eq(ExecutionStatus.FINISHED));
        verify(executionService, times(1)).updateStatus(eq(EXECUTION_ID), eq(ExecutionStatus.PROCESSING));
        verify(environmentService, times(1)).getById(eq(EXECUTION_ENVIRONMENT_ID));
        verify(executionService, times(1)).save(any());
        verify(executionService, times(1)).getByScenarioId(eq(EXECUTION_SCENARIO_ID));
        verify(queryExecutorRegistry, times(1)).getQueryExecutor(eq(ENVIRONMENT_TYPE));

        verify(queryExecutor, never()).execute(any(), any());
    }

    @Test
    void performExecution_ShouldNotExecuteTest_WhenEnvironmentNotFound() {
        ExecutionDto execution = getExecutionDto();

        when(redissonClient.getLock(anyString())).thenReturn(rLock);
        when(environmentService.getById(EXECUTION_ENVIRONMENT_ID)).thenReturn(Optional.empty());
        when(executionService.save(eq(execution))).thenReturn(execution);
        when(executionService.getByScenarioId(eq(EXECUTION_SCENARIO_ID))).thenReturn(List.of(execution));

        executionConsumer.performExecution(execution);

        assertNull(execution.getExecutionTimesMillis());
        assertEquals(0, execution.getSuccessExecutions());
        assertEquals(0, execution.getFailingExecutions());
        assertEquals(execution.getExecutionStatus(), ExecutionStatus.FINISHED.name());
        assertNotNull(execution.getErrors());
        assertEquals(1, execution.getErrors().size());

        verify(redissonClient, times(1)).getLock(anyString());
        verify(rLock, times(1)).lock(anyLong(), any());
        verify(rLock, times(1)).unlock();
        verify(scenarioService, times(1)).updateStatus(eq(EXECUTION_SCENARIO_ID), eq(ExecutionStatus.PROCESSING));
        verify(scenarioService, times(1)).updateStatus(eq(EXECUTION_SCENARIO_ID), eq(ExecutionStatus.FINISHED));
        verify(executionService, times(1)).updateStatus(eq(EXECUTION_ID), eq(ExecutionStatus.PROCESSING));
        verify(environmentService, times(1)).getById(eq(EXECUTION_ENVIRONMENT_ID));
        verify(executionService, times(1)).save(any());
        verify(executionService, times(1)).getByScenarioId(eq(EXECUTION_SCENARIO_ID));

        verify(queryExecutorRegistry, never()).getQueryExecutor(eq(ENVIRONMENT_TYPE));
        verify(queryExecutor, never()).execute(any(), any());
    }

    private QueryDto getQueryDto() {
        return new QueryDto(QUERY_QUERY, QUERY_TIMEOUT_IN_SECONDS, Collections.emptyMap());
    }

    private EnvironmentDto getEnvironmentDto() {
        return new EnvironmentDto(
                ENVIRONMENT_ID, ENVIRONMENT_NAME, ENVIRONMENT_DESCRIPTION, ENVIRONMENT_INSTANCE_URL,
                ENVIRONMENT_CONNECTION_PROPERTIES, ENVIRONMENT_HEALTH_CHECK_QUERY, ENVIRONMENT_TYPE,
                ENVIRONMENT_CREATION_DATE_TIME, ENVIRONMENT_LAST_UPDATE_DATE_TIME
        );
    }

    private ExecutionDto getExecutionDto() {
        return ExecutionDto.builder()
                .id(EXECUTION_ID)
                .scenarioId(EXECUTION_SCENARIO_ID)
                .query(getQueryDto())
                .environmentId(EXECUTION_ENVIRONMENT_ID)
                .environmentName(EXECUTION_ENVIRONMENT_NAME)
                .warmUpIterations(EXECUTION_WARM_UP_ITERATIONS)
                .measurements(EXECUTION_MEASUREMENTS)
                .executionStatus(EXECUTION_EXECUTION_STATUS)
                .build();
    }
}