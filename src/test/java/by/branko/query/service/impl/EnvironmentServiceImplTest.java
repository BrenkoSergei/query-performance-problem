package by.branko.query.service.impl;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.repository.EnvironmentRepository;
import by.branko.query.repository.entity.Environment;
import by.branko.query.service.converter.Converter;
import by.branko.query.service.exception.QueryPerformanceException;
import by.branko.query.service.executor.QueryExecutor;
import by.branko.query.service.executor.QueryExecutorRegistry;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Example;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class EnvironmentServiceImplTest {

    private static final String ID = UUID.randomUUID().toString();
    private static final String NAME = RandomStringUtils.randomAlphabetic(10);
    private static final String DESCRIPTION = RandomStringUtils.randomAlphabetic(10);
    private static final String INSTANCE_URL = RandomStringUtils.randomAlphabetic(10);
    private static final Map<String, String> CONNECTION_PROPERTIES = Collections.emptyMap();
    private static final String HEALTH_CHECK_QUERY = RandomStringUtils.randomAlphabetic(10);
    private static final String TYPE = RandomStringUtils.randomAlphabetic(10);
    private static final LocalDateTime CREATION_DATE_TIME = LocalDateTime.now();
    private static final LocalDateTime LAST_UPDATE_DATE_TIME = LocalDateTime.now();

    @InjectMocks
    private EnvironmentServiceImpl environmentService;

    @Mock
    private QueryExecutorRegistry queryExecutorRegistry;
    @Mock
    private QueryExecutor queryExecutor;
    @Mock
    private EnvironmentRepository environmentRepository;
    @Mock
    private Converter<Environment, EnvironmentDto> environmentConverter;

    @Test
    void getAll_ReturnsAllEnvironments_Positive() {
        when(environmentRepository.findAll()).thenReturn(List.of(getEnvironment()));
        when(environmentConverter.toDtos(anyList())).thenCallRealMethod();
        when(environmentConverter.toDto(eq(getEnvironment()))).thenReturn(getEnvironmentDto());

        List<EnvironmentDto> environments = environmentService.getAll();

        assertNotNull(environments, "Asserting that returned List is non null");
        assertEquals(environments.size(), 1, "Asserting size of returned List");
        assertEquals(environments.get(0), getEnvironmentDto(), "Asserting that the List contains expected object");
        verify(environmentRepository, times(1)).findAll();
        verify(environmentConverter, times(1)).toDto(eq(getEnvironment()));
    }

    @Test
    void getById_ReturnsEnvironment_WhenItFoundById() {
        when(environmentRepository.findById(eq(ID))).thenReturn(Optional.of(getEnvironment()));
        when(environmentConverter.toDto(eq(getEnvironment()))).thenReturn(getEnvironmentDto());

        Optional<EnvironmentDto> maybeEnvironment = environmentService.getById(ID);

        assertTrue(maybeEnvironment.isPresent(), "Asserting the Environment presence");
        assertEquals(maybeEnvironment.get(), getEnvironmentDto(), "Asserting the Environment equality");
        verify(environmentRepository, times(1)).findById(eq(ID));
        verify(environmentConverter, times(1)).toDto(getEnvironment());
    }

    @Test
    void getById_ReturnsEmptyOptional_WhenEnvironmentNotFound() {
        when(environmentRepository.findById(eq(ID))).thenReturn(Optional.empty());
        when(environmentConverter.toDto(eq(getEnvironment()))).thenReturn(getEnvironmentDto());

        Optional<EnvironmentDto> maybeEnvironment = environmentService.getById(ID);

        assertTrue(maybeEnvironment.isEmpty(), "Asserting the Environment absence");
        verify(environmentRepository, times(1)).findById(eq(ID));
        verify(environmentConverter, never()).toDto(getEnvironment());
    }

    @Test
    void getByNames_ReturnsFoundEnvironments_Positive() {
        when(environmentRepository.findByNameIn(List.of(NAME))).thenReturn(List.of(getEnvironment()));
        when(environmentConverter.toDtos(anyList())).thenCallRealMethod();
        when(environmentConverter.toDto(eq(getEnvironment()))).thenReturn(getEnvironmentDto());

        List<EnvironmentDto> environments = environmentService.getByNames(List.of(NAME));

        assertNotNull(environments, "Asserting that returned List is non null");
        assertEquals(environments.size(), 1, "Asserting size of returned List");
        assertEquals(environments.get(0), getEnvironmentDto(), "Asserting that the List contains expected object");
        verify(environmentRepository, times(1)).findByNameIn(List.of(NAME));
        verify(environmentConverter, times(1)).toDto(eq(getEnvironment()));
    }

    @Test
    void searchByName() {
        when(environmentRepository.findByNameLike(NAME)).thenReturn(List.of(getEnvironment()));
        when(environmentConverter.toDtos(anyList())).thenCallRealMethod();
        when(environmentConverter.toDto(eq(getEnvironment()))).thenReturn(getEnvironmentDto());

        List<EnvironmentDto> environments = environmentService.searchByName(NAME);

        assertNotNull(environments, "Asserting that returned List is non null");
        assertEquals(environments.size(), 1, "Asserting size of returned List");
        assertEquals(environments.get(0), getEnvironmentDto(), "Asserting that the List contains expected object");
        verify(environmentRepository, times(1)).findByNameLike(NAME);
        verify(environmentConverter, times(1)).toDto(eq(getEnvironment()));
    }

    @Test
    void save_ReturnsSavedEntity_WhenValidAndIdIsNotProvided() {
        Environment environment = getEnvironment();
        EnvironmentDto environmentDto = getEnvironmentDto();
        environment.setId(null);
        environmentDto.setId(null);

        when(environmentConverter.toEntity(eq(environmentDto))).thenReturn(environment);
        when(environmentRepository.findAll(any(Example.class))).thenReturn(Collections.emptyList());
        when(queryExecutorRegistry.getQueryExecutor(eq(TYPE))).thenReturn(Optional.of(queryExecutor));
        when(queryExecutor.isHealth(eq(environmentDto))).thenReturn(Boolean.TRUE);
        when(environmentRepository.save(environment)).thenReturn(environment);
        when(environmentConverter.toDto(eq(environment))).thenReturn(environmentDto);

        EnvironmentDto savedEnvironmentDto = environmentService.save(environmentDto);

        assertEquals(environmentDto, savedEnvironmentDto);
        verify(environmentConverter, times(1)).toEntity(eq(environmentDto));
        verify(environmentRepository, times(1)).findAll(any(Example.class));
        verify(queryExecutorRegistry, times(1)).getQueryExecutor(eq(TYPE));
        verify(queryExecutor, times(1)).isHealth(eq(environmentDto));
        verify(environmentRepository, times(1)).save(environment);
        verify(environmentConverter, times(1)).toDto(eq(environment));
    }

    @Test
    void save_ReturnsSavedEntity_WhenValidAndIdIsProvided() {
        when(environmentConverter.toEntity(eq(getEnvironmentDto()))).thenReturn(getEnvironment());
        when(environmentRepository.existsById(eq(ID))).thenReturn(Boolean.TRUE);
        when(environmentRepository.findAll(any(Example.class))).thenReturn(Collections.emptyList());
        when(queryExecutorRegistry.getQueryExecutor(eq(TYPE))).thenReturn(Optional.of(queryExecutor));
        when(queryExecutor.isHealth(eq(getEnvironmentDto()))).thenReturn(Boolean.TRUE);
        when(environmentRepository.save(getEnvironment())).thenReturn(getEnvironment());
        when(environmentConverter.toDto(eq(getEnvironment()))).thenReturn(getEnvironmentDto());

        EnvironmentDto savedEnvironmentDto = environmentService.save(getEnvironmentDto());

        assertEquals(getEnvironmentDto(), savedEnvironmentDto);
        verify(environmentConverter, times(1)).toEntity(eq(getEnvironmentDto()));
        verify(environmentRepository, times(1)).existsById(eq(ID));
        verify(environmentRepository, times(1)).findAll(any(Example.class));
        verify(queryExecutorRegistry, times(1)).getQueryExecutor(eq(TYPE));
        verify(queryExecutor, times(1)).isHealth(eq(getEnvironmentDto()));
        verify(environmentRepository, times(1)).save(getEnvironment());
        verify(environmentConverter, times(1)).toDto(eq(getEnvironment()));
    }

    @Test
    void save_ThrowsException_WhenEntityWithProvidedIdNotFound() {
        when(environmentConverter.toEntity(eq(getEnvironmentDto()))).thenReturn(getEnvironment());
        when(environmentRepository.existsById(eq(ID))).thenReturn(Boolean.FALSE);

        assertThrows(QueryPerformanceException.class, () -> environmentService.save(getEnvironmentDto()));

        verify(environmentConverter, times(1)).toEntity(eq(getEnvironmentDto()));
        verify(environmentRepository, times(1)).existsById(eq(ID));
    }

    @Test
    void save_ThrowsException_WhenEnvironmentIsNotHealth() {
        when(environmentConverter.toEntity(eq(getEnvironmentDto()))).thenReturn(getEnvironment());
        when(environmentRepository.existsById(eq(ID))).thenReturn(Boolean.TRUE);
        when(environmentRepository.findAll(any(Example.class))).thenReturn(Collections.emptyList());
        when(queryExecutorRegistry.getQueryExecutor(eq(TYPE))).thenReturn(Optional.of(queryExecutor));
        when(queryExecutor.isHealth(eq(getEnvironmentDto()))).thenReturn(Boolean.FALSE);

        assertThrows(QueryPerformanceException.class, () -> environmentService.save(getEnvironmentDto()));

        verify(environmentConverter, times(1)).toEntity(eq(getEnvironmentDto()));
        verify(environmentRepository, times(1)).existsById(eq(ID));
        verify(environmentRepository, times(1)).findAll(any(Example.class));
        verify(queryExecutorRegistry, times(1)).getQueryExecutor(eq(TYPE));
        verify(queryExecutor, times(1)).isHealth(eq(getEnvironmentDto()));
    }

    @Test
    void delete_CallsDeletion_WhenEnvironmentExists() {
        when(environmentRepository.existsById(eq(ID))).thenReturn(Boolean.TRUE);
        doNothing().when(environmentRepository).deleteById(eq(ID));

        environmentService.delete(ID);

        verify(environmentRepository, times(1)).existsById(eq(ID));
        verify(environmentRepository, times(1)).deleteById(eq(ID));
    }

    @Test
    void delete_ThrowsException_WhenEnvironmentNotFound() {
        when(environmentRepository.existsById(eq(ID))).thenReturn(Boolean.FALSE);
        doNothing().when(environmentRepository).deleteById(eq(ID));

        assertThrows(QueryPerformanceException.class, () -> environmentService.delete(ID));

        verify(environmentRepository, times(1)).existsById(eq(ID));
        verify(environmentRepository, never()).deleteById(eq(ID));
    }

    private Environment getEnvironment() {
        return new Environment(
                ID, NAME, DESCRIPTION, INSTANCE_URL, CONNECTION_PROPERTIES, HEALTH_CHECK_QUERY, TYPE,
                CREATION_DATE_TIME, LAST_UPDATE_DATE_TIME
        );
    }

    private EnvironmentDto getEnvironmentDto() {
        return new EnvironmentDto(
                ID, NAME, DESCRIPTION, INSTANCE_URL, CONNECTION_PROPERTIES, HEALTH_CHECK_QUERY, TYPE,
                CREATION_DATE_TIME, LAST_UPDATE_DATE_TIME
        );
    }
}