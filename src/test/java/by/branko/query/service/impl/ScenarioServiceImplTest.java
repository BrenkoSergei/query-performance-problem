package by.branko.query.service.impl;

import by.branko.query.domain.EnvironmentDto;
import by.branko.query.domain.ExecutionDto;
import by.branko.query.domain.QueryDto;
import by.branko.query.domain.ScenarioDto;
import by.branko.query.repository.ScenarioRepository;
import by.branko.query.repository.entity.ExecutionStatus;
import by.branko.query.repository.entity.Query;
import by.branko.query.repository.entity.Scenario;
import by.branko.query.service.EnvironmentService;
import by.branko.query.service.ExecutionService;
import by.branko.query.service.KafkaSubmissionService;
import by.branko.query.service.converter.Converter;
import by.branko.query.service.exception.QueryPerformanceException;
import by.branko.query.service.executor.QueryExecutor;
import by.branko.query.service.executor.QueryExecutorRegistry;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ScenarioServiceImplTest {

    private static final LocalDateTime SCENARIO_SUBMISSION_DATE_TIME = LocalDateTime.now();
    private static final String QUERY_1_QUERY = RandomStringUtils.randomAlphabetic(10);
    private static final int QUERY_1_TIMEOUT_IN_SECONDS = 10;

    private static final String QUERY_2_QUERY = RandomStringUtils.randomAlphabetic(10);
    private static final int QUERY_2_TIMEOUT_IN_SECONDS = 10;

    private static final String SCENARIO_ID = UUID.randomUUID().toString();
    private static final String SCENARIO_DESCRIPTION = RandomStringUtils.randomAlphabetic(10);
    private static final String SCENARIO_ENVIRONMENT_ID_1 = UUID.randomUUID().toString();
    private static final String SCENARIO_ENVIRONMENT_ID_2 = UUID.randomUUID().toString();
    private static final String SCENARIO_ENVIRONMENT_NAME_1 = RandomStringUtils.randomAlphabetic(10);
    private static final String SCENARIO_ENVIRONMENT_NAME_2 = RandomStringUtils.randomAlphabetic(10);
    private static final String SCENARIO_ENVIRONMENT_TYPE = RandomStringUtils.randomAlphabetic(10);
    private static final int SCENARIO_WARM_UP_ITERATIONS = 10;
    private static final int SCENARIO_MEASUREMENT = 10;
    private static final ExecutionStatus SCENARIO_EXECUTION_STATUS = ExecutionStatus.PENDING;
    private static final LocalDateTime SCENARIO_LAST_ACTION_DATE_TIME = LocalDateTime.now();

    @InjectMocks
    private ScenarioServiceImpl scenarioService;

    @Mock
    private ScenarioRepository scenarioRepository;
    @Mock
    private ExecutionService executionService;
    @Mock
    private EnvironmentService environmentService;
    @Mock
    private QueryExecutorRegistry queryExecutorRegistry;
    @Mock
    private QueryExecutor queryExecutor;
    @Mock
    private KafkaSubmissionService kafkaSubmissionService;
    @Mock
    private Converter<Scenario, ScenarioDto> executionScenarioConverter;

    @Test
    void submit_ReturnsSubmittedScenario_WhenNoErrorsOccurred() {
        Scenario scenario = getScenario();
        ScenarioDto scenarioDto = getScenarioDto();
        List<ExecutionDto> executions = List.of(getExecutionDto1(), getExecutionDto2(), getExecutionDto3(), getExecutionDto4());

        when(environmentService.getByNames(Set.of(SCENARIO_ENVIRONMENT_NAME_1, SCENARIO_ENVIRONMENT_NAME_2)))
                .thenReturn(List.of(getEnvironmentDto1(), getEnvironmentDto2()));
        when(queryExecutorRegistry.getQueryExecutor(SCENARIO_ENVIRONMENT_TYPE)).thenReturn(Optional.of(queryExecutor));
        when(queryExecutor.isHealth(any())).thenReturn(Boolean.TRUE);
        when(executionScenarioConverter.toEntity(scenarioDto)).thenReturn(scenario);
        when(scenarioRepository.save(scenario)).thenReturn(scenario);
        when(executionScenarioConverter.toDto(scenario)).thenReturn(scenarioDto);
        when(executionService.saveAll(anyList())).thenReturn(executions);
        doNothing().when(kafkaSubmissionService).submit(anyString(), anyString(), any());

        ScenarioDto savedScenarioDto = scenarioService.submit(scenarioDto);

        assertEquals(scenarioDto, savedScenarioDto);
        verify(environmentService, times(1)).getByNames(anySet());
        verify(queryExecutorRegistry, times(2)).getQueryExecutor(SCENARIO_ENVIRONMENT_TYPE);
        verify(queryExecutor, times(2)).isHealth(any());
        verify(executionScenarioConverter, times(1)).toEntity(scenarioDto);
        verify(scenarioRepository, times(1)).save(scenario);
        verify(executionScenarioConverter, times(1)).toDto(scenario);
        verify(executionService, times(1)).saveAll(anyList());
        verify(kafkaSubmissionService, times(executions.size())).submit(anyString(), anyString(), any());
    }

    @Test
    void submit_ThrowsException_WhenEnvironmentNotFound() {
        ScenarioDto scenarioDto = getScenarioDto();

        when(environmentService.getByNames(anySet())).thenReturn(Collections.emptyList());
        when(queryExecutorRegistry.getQueryExecutor(SCENARIO_ENVIRONMENT_TYPE)).thenReturn(Optional.of(queryExecutor));

        assertThrows(QueryPerformanceException.class, () -> scenarioService.submit(scenarioDto));

        verify(environmentService, times(1)).getByNames(anySet());
        verify(queryExecutorRegistry, never()).getQueryExecutor(SCENARIO_ENVIRONMENT_TYPE);
    }

    @Test
    void submit_ThrowsException_WhenEnvironmentIsNotHealth() {
        ScenarioDto scenarioDto = getScenarioDto();

        when(environmentService.getByNames(Set.of(SCENARIO_ENVIRONMENT_NAME_1, SCENARIO_ENVIRONMENT_NAME_2)))
                .thenReturn(List.of(getEnvironmentDto1(), getEnvironmentDto2()));
        when(queryExecutorRegistry.getQueryExecutor(SCENARIO_ENVIRONMENT_TYPE)).thenReturn(Optional.of(queryExecutor));
        when(queryExecutor.isHealth(any())).thenReturn(Boolean.FALSE);

        assertThrows(QueryPerformanceException.class, () -> scenarioService.submit(scenarioDto));

        verify(environmentService, times(1)).getByNames(anySet());
        verify(queryExecutorRegistry, times(1)).getQueryExecutor(SCENARIO_ENVIRONMENT_TYPE);
        verify(queryExecutor, times(1)).isHealth(any());
    }

    private QueryDto getQueryDto1() {
        return new QueryDto(QUERY_1_QUERY, QUERY_1_TIMEOUT_IN_SECONDS, Collections.emptyMap());
    }

    private QueryDto getQueryDto2() {
        return new QueryDto(QUERY_2_QUERY, QUERY_2_TIMEOUT_IN_SECONDS, Collections.emptyMap());
    }

    private Query getQuery1() {
        return new Query(QUERY_1_QUERY, QUERY_1_TIMEOUT_IN_SECONDS, Collections.emptyMap());
    }

    private Query getQuery2() {
        return new Query(QUERY_2_QUERY, QUERY_2_TIMEOUT_IN_SECONDS, Collections.emptyMap());
    }

    private EnvironmentDto getEnvironmentDto1() {
        EnvironmentDto environmentDto = new EnvironmentDto();
        environmentDto.setId(SCENARIO_ENVIRONMENT_ID_1);
        environmentDto.setName(SCENARIO_ENVIRONMENT_NAME_1);
        environmentDto.setType(SCENARIO_ENVIRONMENT_TYPE);
        return environmentDto;
    }

    private EnvironmentDto getEnvironmentDto2() {
        EnvironmentDto environmentDto = new EnvironmentDto();
        environmentDto.setId(SCENARIO_ENVIRONMENT_ID_2);
        environmentDto.setName(SCENARIO_ENVIRONMENT_NAME_2);
        environmentDto.setType(SCENARIO_ENVIRONMENT_TYPE);
        return environmentDto;
    }

    private ScenarioDto getScenarioDto() {
        return new ScenarioDto(
                SCENARIO_ID, SCENARIO_DESCRIPTION, List.of(getQueryDto1(), getQueryDto2()),
                List.of(SCENARIO_ENVIRONMENT_NAME_1, SCENARIO_ENVIRONMENT_NAME_2), SCENARIO_WARM_UP_ITERATIONS,
                SCENARIO_MEASUREMENT, SCENARIO_EXECUTION_STATUS.name(),
                SCENARIO_SUBMISSION_DATE_TIME, SCENARIO_LAST_ACTION_DATE_TIME
        );
    }

    private Scenario getScenario() {
        return new Scenario(
                SCENARIO_ID, SCENARIO_DESCRIPTION, List.of(getQuery1(), getQuery2()),
                List.of(SCENARIO_ENVIRONMENT_NAME_1, SCENARIO_ENVIRONMENT_NAME_2), SCENARIO_WARM_UP_ITERATIONS,
                SCENARIO_MEASUREMENT, SCENARIO_EXECUTION_STATUS, SCENARIO_SUBMISSION_DATE_TIME, SCENARIO_LAST_ACTION_DATE_TIME
        );
    }

    private ExecutionDto getExecutionDto1() {
        return ExecutionDto.builder()
                .id(UUID.randomUUID().toString())
                .scenarioId(SCENARIO_ID)
                .query(getQueryDto1())
                .environmentId(SCENARIO_ENVIRONMENT_ID_1)
                .environmentName(SCENARIO_ENVIRONMENT_NAME_1)
                .warmUpIterations(SCENARIO_WARM_UP_ITERATIONS)
                .measurements(SCENARIO_MEASUREMENT)
                .executionStatus(SCENARIO_EXECUTION_STATUS.name())
                .build();
    }

    private ExecutionDto getExecutionDto2() {
        return ExecutionDto.builder()
                .id(UUID.randomUUID().toString())
                .scenarioId(SCENARIO_ID)
                .query(getQueryDto2())
                .environmentId(SCENARIO_ENVIRONMENT_ID_1)
                .environmentName(SCENARIO_ENVIRONMENT_NAME_1)
                .warmUpIterations(SCENARIO_WARM_UP_ITERATIONS)
                .measurements(SCENARIO_MEASUREMENT)
                .executionStatus(SCENARIO_EXECUTION_STATUS.name())
                .build();
    }

    private ExecutionDto getExecutionDto3() {
        return ExecutionDto.builder()
                .id(UUID.randomUUID().toString())
                .scenarioId(SCENARIO_ID)
                .query(getQueryDto1())
                .environmentId(SCENARIO_ENVIRONMENT_ID_2)
                .environmentName(SCENARIO_ENVIRONMENT_NAME_2)
                .warmUpIterations(SCENARIO_WARM_UP_ITERATIONS)
                .measurements(SCENARIO_MEASUREMENT)
                .executionStatus(SCENARIO_EXECUTION_STATUS.name())
                .build();
    }

    private ExecutionDto getExecutionDto4() {
        return ExecutionDto.builder()
                .id(UUID.randomUUID().toString())
                .scenarioId(SCENARIO_ID)
                .query(getQueryDto2())
                .environmentId(SCENARIO_ENVIRONMENT_ID_2)
                .environmentName(SCENARIO_ENVIRONMENT_NAME_2)
                .warmUpIterations(SCENARIO_WARM_UP_ITERATIONS)
                .measurements(SCENARIO_MEASUREMENT)
                .executionStatus(SCENARIO_EXECUTION_STATUS.name())
                .build();
    }
}